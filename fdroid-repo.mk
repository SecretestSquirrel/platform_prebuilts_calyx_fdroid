# Auto generated, do not edit.

PRODUCT_COPY_FILES += \
    prebuilts/calyx/fdroid/fallback-icon.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/fallback-icon.png \
    prebuilts/calyx/fdroid/repo/categories.txt:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/categories.txt \
    prebuilts/calyx/fdroid/repo/icons-120/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-120/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-120/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-120/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-120/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-120/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-120/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-160/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-160/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-160/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-160/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-160/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-160/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-160/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-240/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-240/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-240/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-240/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-240/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-240/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-240/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-320/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-320/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-320/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-320/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-320/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-320/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-320/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-480/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-480/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-480/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-480/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-480/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-480/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-480/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-640/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons-640/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons-640/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons-640/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons-640/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons-640/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons-640/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/icons/app.organicmaps.21052106.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/app.organicmaps.21052106.png \
    prebuilts/calyx/fdroid/repo/icons/com.artifex.mupdf.viewer.app.90.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.artifex.mupdf.viewer.app.90.png \
    prebuilts/calyx/fdroid/repo/icons/com.aurora.store.36.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.aurora.store.36.png \
    prebuilts/calyx/fdroid/repo/icons/com.duckduckgo.mobile.android.58602.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.duckduckgo.mobile.android.58602.png \
    prebuilts/calyx/fdroid/repo/icons/com.jarsilio.android.scrambledeggsif.66.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.jarsilio.android.scrambledeggsif.66.png \
    prebuilts/calyx/fdroid/repo/icons/com.nextcloud.client.30160190.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.nextcloud.client.30160190.png \
    prebuilts/calyx/fdroid/repo/icons/com.yubico.yubioath.20199.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/com.yubico.yubioath.20199.png \
    prebuilts/calyx/fdroid/repo/icons/eu.siacs.conversations.42015.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/eu.siacs.conversations.42015.png \
    prebuilts/calyx/fdroid/repo/icons/fdroid-icon.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/fdroid-icon.png \
    prebuilts/calyx/fdroid/repo/icons/info.guardianproject.locationprivacy.31.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/info.guardianproject.locationprivacy.31.png \
    prebuilts/calyx/fdroid/repo/icons/org.briarproject.briar.android.10220.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.briarproject.briar.android.10220.png \
    prebuilts/calyx/fdroid/repo/icons/org.calyxinstitute.vpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.calyxinstitute.vpn.149.png \
    prebuilts/calyx/fdroid/repo/icons/org.dmfs.tasks.82200.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.dmfs.tasks.82200.png \
    prebuilts/calyx/fdroid/repo/icons/org.secuso.privacyfriendlyweather.13.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.secuso.privacyfriendlyweather.13.png \
    prebuilts/calyx/fdroid/repo/icons/org.sufficientlysecure.keychain.57500.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.sufficientlysecure.keychain.57500.png \
    prebuilts/calyx/fdroid/repo/icons/org.thoughtcrime.securesms.85100.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.thoughtcrime.securesms.85100.png \
    prebuilts/calyx/fdroid/repo/icons/org.torproject.torbrowser.2015808075.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/org.torproject.torbrowser.2015808075.png \
    prebuilts/calyx/fdroid/repo/icons/se.leap.riseupvpn.149.png:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/icons/se.leap.riseupvpn.149.png \
    prebuilts/calyx/fdroid/repo/index.jar:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/index.jar \
    prebuilts/calyx/fdroid/repo/index-v1.jar:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/index-v1.jar \
    prebuilts/calyx/fdroid/repo/index-v1.json:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/index-v1.json \
    prebuilts/calyx/fdroid/repo/index.xml:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/index.xml \
    prebuilts/calyx/fdroid/repo/status/running.json:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/status/running.json \
    prebuilts/calyx/fdroid/repo/status/update.json:$(TARGET_COPY_OUT_PRODUCT)/fdroid/repo/status/update.json \

PRODUCT_PACKAGES += \
    fdroid-repo \
    AuroraStore \
    Bitwarden \
    Briar \
    CalyxVPN \
    Conversations \
    DAVx5 \
    DuckDuckGoPrivacyBrowser \
    K-9Mail \
    LocationPrivacy \
    MuPDFviewer \
    Nextcloud \
    OONIProbe \
    OpenKeychain \
    Orbot \
    OrganicMaps \
    RiseupVPN \
    ScrambledExif \
    Signal \
    Tasks \
    TorBrowser \
    Weather \
    YubicoAuthenticator \
